# Build a simple calculator.

Ask the user what the first number is, then ask if they want to multiply, divide, subtract, or add. If division, ask if they want floating point division, or truncadet division. Then process the request.

Allow the user to type `q` to quit.
