
def addition(first_number, second_number):
    solution = float(first_number) + float(second_number)
    return solution


def subtraction(first_number, second_number):
    return float(first_number) - float(second_number)


def multiplication(first_number, second_number):
    return float(first_number) * float(second_number)


def floating_point_division(first_number, second_number):
    return float(first_number) / float(second_number)


def truncating_division(first_number, second_number):
    return int(float(first_number) // float(second_number))


def modulus(first_number, second_number):
    return int(float(first_number) % float(second_number))


def exponentiation(first_number, second_number):
    return float(first_number) ** float(second_number)


def calculator():
    print('Welcome to the calculator.')
    while True:
        print('Enter the first number, the math , then another number')
        first_number = input("Enter the first number [type q to quit]: ")
        if first_number == 'q':
            break
        math_function = input(
            'How would you like to math? (valid functions are "+", "-", "/", "//", "%", or "**": ')
        second_number = input('Second number: ')
        if math_function == '+':
            print(addition(first_number, second_number))
        elif math_function == '-':
            print(subtraction(first_number, second_number))
        elif math_function == '/':
            print(floating_point_division(first_number, second_number))
        elif math_function == '//':
            print(truncating_division(first_number, second_number))
        elif math_function == '%':
            print(modulus(first_number, second_number))
        elif math_function == '**':
            print(exponentiation(first_number, second_number))
        else:
            print('Did not understand the math function, please try again.')


if __name__ == "__main__":
    calculator()
