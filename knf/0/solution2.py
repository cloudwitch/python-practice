#!/usr/bin/env python3

def check_if_divisible(int1, int2=2):
    if int1 % int2 == 0:
        print(f"{int1} is divisible by {int2}!")
    else:
        print(f"{int1} is not divisible by {int2}!")

firstint = int(input('What number would you like to check?'))
divisor = int(input('What is your divisor?'))
check_if_divisible(firstint, divisor)
