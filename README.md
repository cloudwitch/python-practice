# Python Practice

This is a place where my partner and I can practice Python. It will largely be based on a combination of the book Introducing Python: Modern computing in Simple Packages and MIT's Open Courseware 6.0001 course.

## How to get started:

Have git installed. This will assume you are running Ubuntu.

```
sudo apt update && sudo apt -y install git
```

Create a directory called `git` in your home directorty. This is where we will clone everything to.

```
mkdir ~/git
```

**Note:** `~` is a shortcut do your home directory who's full path looks something like this:

```
$ cd ~
$ pwd
/home/fiona
$ cd /home/fiona/
$ pwd
/home/fiona
```

Change directory into the one we just created.

```
cd ~/git
```

Clone the project.

If you have a gitlab account, use SSH.

```
git clone git@gitlab.com:cloudwitch/python-practice.git
```

If you do not have a gitlab account, use HTTP:

```
https://gitlab.com/cloudwitch/python-practice.git
```

Change directory into the one we just cloned.

```
cd python-practice
```

Open Visual Studio Code.

```
code .
```
